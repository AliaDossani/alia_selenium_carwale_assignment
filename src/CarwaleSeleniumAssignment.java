import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;

public class CarwaleSeleniumAssignment {
	public static void main(String[] args) throws InterruptedException, IOException {
		
		// Constants
		String BASE_URL = "https://www.carwale.com";
		String WEB_DRIVER = "webdriver.chrome.driver";
		String WEB_DRIVER_PATH = "/usr/local/bin/chromedriver";
		String OUTPUT_EXCEL_FILENAME = "Carwale.xlsx";
		String OUTPUT_EXCEL_SHEETNAME = "Carwale Filtered List Output";
		
		// XPaths and Classnames
		String NEW_CARS_XPATH = "/html[1]/body[1]/div[5]/nav[1]/div[1]/ul[1]/li[1]/div[1]/span[1]";
		String FIND_NEW_CAR_XPATH = "/html[1]/body[1]/div[5]/nav[1]/div[1]/ul[1]/li[1]/div[2]/div[1]/div[1]/div[1]/ul[1]/li[1]/a[1]";
		String BODY_TYPE_XPATH = "/html[1]/body[1]/div[9]/section[1]/div[1]/div[1]/div[1]/ul[1]/li[3]/h3[1]";
		String SUV_MUV_XPATH = "/html[1]/body[1]/div[9]/section[1]/div[1]/div[1]/div[4]/div[1]/div[1]/ul[1]/li[4]";
		String BUDGET_FILTER_XPATH = "/html[1]/body[1]/div[13]/section[2]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/h3[1]";
		String AMOUNT_BUDGET_XPATH = "/html[1]/body[1]/div[13]/section[2]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[5]/a[1]";
		String PAGE_LOADER_XPATH = "/html[1]/body[1]/div[6]";
		String FILTERED_RESULTS_TABLE_ID = "tbl_res";
		String VERSION_ROW_CLASS_NAME = "ver-pdd";
		String MODEL_NAME_KEY = "href-title";
		String MODEL_PRICE_KEY = "new-price2";
		String VERSION_ACCORDION_OPEN_KEY = "viewVersions";
		
		// Initialize Excel Sheet Parameters
		XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(OUTPUT_EXCEL_SHEETNAME);
		
		/*
		 * Step 1: Initialize web Driver with 2 properties and set BASE_URL
		 * 1. WEB_DRIVER: The web driver used for the automation test.
		 * 2. WEB_DRIVER_PATH: The path on system for the selected web driver.
		 */
		
    	System.setProperty(WEB_DRIVER, WEB_DRIVER_PATH);
		WebDriver driver = new ChromeDriver();
        driver.get(BASE_URL);

        
       /*
        * Step 2: Hover over New Cars and select Find New Cars in the sub menu.
        * Use actions to movetoElement: NEW_CARS_XPATH, causing a hover (moveToElement) and expanding of menu.
        * Explicitly wait until find new cars is clickable using WebDriverWait and click to route to new page.
        */
        
        Actions actions = new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver, 100);
        
        WebElement new_cars = driver.findElement(By.xpath(NEW_CARS_XPATH));
        actions.moveToElement(new_cars).build().perform();
        
        WebElement find_new_cars = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(FIND_NEW_CAR_XPATH)));
		find_new_cars.click();
		
		/*
		 * Step 3: Select Tab Body Type and click on SUV / MUV sub category.
		 * Reuse the WebDriverWait class method until to make sure that the element is present
		 * and interactive for both, tab as well as sub category.  
		 */
		
		WebElement body_type_tab = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BODY_TYPE_XPATH)));
		body_type_tab.click();
	       
		WebElement suv_muv_cars = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(SUV_MUV_XPATH)));
		suv_muv_cars.click();
		
		/*
		 * Step 4: Open Filter Budget and select particular filter.
		 * Wait for the new list to load
		 */
		
		WebElement bubget_filter = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(BUDGET_FILTER_XPATH)));
		bubget_filter.click();
		
		WebElement amount_budget = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(AMOUNT_BUDGET_XPATH)));
		amount_budget.click();
		
		// Wait for API call to finish and hide loader
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(PAGE_LOADER_XPATH)));
		
		/*
		 * Step 5: Read the following data from search results, and save in an excel sheet.
		 * Model name, Price, Respective version names 
		 * 
		 *  Find DOM elements to fetch Model Name, Price and Version Names for all rows
		 *  Logic for Versions: The id of each toggle button is used to identify child version of each row.
		 *  For eg. If id=224, then all car versions will have class name as cls_224. Concat all of them into a string.
		 */
		
		WebElement filtered_results = driver.findElement(By.id(FILTERED_RESULTS_TABLE_ID));
		List<WebElement> MainRow = wait.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(filtered_results, By.className("model-row")));
        
        int rowCount = 0;
		for(WebElement elem : MainRow) {
			Row row = sheet.createRow(rowCount++);
	        
			WebElement toggle_version_view = elem.findElement(By.className(VERSION_ACCORDION_OPEN_KEY));
			toggle_version_view.click();
			String rowId = toggle_version_view.getAttribute("id");

	        List<WebElement> car_versions = driver.findElements(By.className("cls_" + rowId));
	        String version_names_collection = "";
	        for (WebElement car_version : car_versions) {
	        	String version_name = car_version.
	        			findElement(By.className(VERSION_ROW_CLASS_NAME)).
	        			findElement(By.tagName("a")).getText();
	        	version_names_collection = version_names_collection.concat(version_name).concat(", ");
	        }
	        
	        // Populate Excel Row with Model Name, Price and Version Names
	        int columnCount = 0;
	        
	        Cell model_name_cell = row.createCell(columnCount++);
	        String model_name = elem.findElement(By.className(MODEL_NAME_KEY)).getText();
	        model_name_cell.setCellValue(model_name);
	        
	        Cell price_cell = row.createCell(columnCount++);
	        String price = elem.findElement(By.className(MODEL_PRICE_KEY)).getText();
	        price_cell.setCellValue(price);

	        Cell version_names_cell = row.createCell(columnCount++);
	        version_names_cell.setCellValue(version_names_collection);
	        
		}
		
		/*
		 *  Save the Workbook Stream to Excel sheet
		 *  Provide the full URL and file name: Carwale.xlsx
		 */
		try (FileOutputStream outputStream = new FileOutputStream(OUTPUT_EXCEL_FILENAME)) {
            workbook.write(outputStream);
        } catch (Exception e) {
        	throw e;
        }

		
        driver.close();
       
    }
}
